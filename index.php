<?php include_once('header.php');?>

<!-- HOME SLIDER -->
<div class="home-slider-section">
	<div class="home-slider">
		<div class="slide-item slide-1"></div>
		<div class="slide-item slide-2"></div>
		<div class="slide-item slide-3"></div>
		<div class="slide-item slide-4"></div>
	</div>
</div>
<!-- ABOUT US -->
<div id="aboutSection" class="home-content">
	<div class="home-section">
		<h2>ABOUT US</h2>
		<span class="text-overlay">ABOUT US</span>
		<div class="hr-outline"></div>
		<p>Events Concierge serves as the premiere gateway to all event needs. We provide quality event suppliers whatever the occasion.</p>
		<p>Owned and managed by Claire S. Villaflor and Eltesa D. Beckett</p>
		<p>Ms. Eltesa D. Beckett of Beckett’s Bridal, one of Cebu’s trusted and sought after names in quality, elegant and reasonable Gowns rentals and creations, creating gowns and groom’s ensemble since 2001.</p>
		<p>Ms. Claire S. Villaflor, of Right Connect Events and Lifetime Events Depot. Founder of the Association of Cebu’s Wedding Planners and Coordinators and is an active member and certified events and wedding coordinator of the Philippine Association Of wedding Planners serving couples since 2005.</p>
		<p class="big">Names with Passion, Service and Integrity.</p>
	</div>
</div>
<!-- WEDDINGS -->
<div id="weddingSection" class="home-banner weddings"></div>
<div class="home-content">
	<div class="home-section">
		<h2>WEDDINGS</h2>
		<span class="text-overlay">WEDDINGS</span>
		<div class="hr-outline"></div>
		<p>Simply elegant.</p>
	</div>
	<div class="events-img-wrapper">
		<img src="/events-concierge/assets/images/weddings-img1.png" alt="Wedding Image 1" />
		<img src="/events-concierge/assets/images/weddings-img2.png" alt="Wedding Image 2" />
		<img src="/events-concierge/assets/images/weddings-img3.png" alt="Wedding Image 3" />
	</div>
	<a href="wedding-packages" class="button">View Wedding Packages</a>
</div>
<!-- DEBUTS -->
<div id="debutSection" class="home-banner debuts"></div>
<div class="home-content">
	<div class="home-section">
		<h2>DEBUTS</h2>
		<span class="text-overlay">DEBUTS</span>
		<div class="hr-outline"></div>
		<p>Shine your brightest. Be like a star!</p>
	</div>
	<div class="events-img-wrapper">
		<img src="/events-concierge/assets/images/debuts-img1.png" alt="Wedding Image 1" />
		<img src="/events-concierge/assets/images/debuts-img2.png" alt="Wedding Image 2" />
		<img src="/events-concierge/assets/images/debuts-img3.png" alt="Wedding Image 3" />
	</div>
	<a href="debut-packages" class="button">View Debut Packages</a>
</div>
<!-- TESTIMONIALS -->
<div class="testimonial-slider-section">
	<div class="testimonial-slider">
		<div class="testimonial-slide">
			<div class="hr-outline"></div>
			<p>"Ms Claire and her staff was just helpful in every way. Awesome team!!!"</p>
			<h4>Carla de los Cientos-Comeros</h4>
		</div>
		<div class="testimonial-slide">
			<div class="hr-outline"></div>
			<p>"A bride can only handle so much stress and anxiety on her wedding day.
			Lucky for me, Claire and her staff were very professional and dependable. 
			I was really happy and thankful they were there with me during our wedding. 
			Wish you more luck and success!!"</p>
			<h4>Ann P.</h4>
		</div>
	</div>
</div>

<?php include_once('footer.php'); ?>