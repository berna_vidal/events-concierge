<?php include_once('header.php');?>
<div class="content-wrapper">
	<div class="container">
		<h1>Wedding Packages</h1>
		<div class="row package-item">
			<div class="col-xs-12 col-sm-3">
				<img src="assets/images/Wedding/162932_491820576871_7612386_n.jpg" alt="So Happy Together Package"/>
			</div>
			<div class="col-xs-12 col-sm-9 package-info-wrapper">
				<div class="package-info">
					<h2>So Happy Together Package</h2>
					<ul>
						<li>Papers</li>
						<li>Preparation/Dress up</li>
						<li>Church</li>
						<li>Reception</li>
						<li>Make up services, Photo/Video services</li>
					</ul>
				</div>
				<a href=""><i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</div>
<?php include_once('footer.php');?>