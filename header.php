<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/header.css">
	<link rel="stylesheet" href="assets/css/footer.css">
	<link rel="stylesheet" href="assets/css/style.css">
	
	<!--JS-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script>$.fn.modal || document.write('<script src="assets/js/jquery.min.js">\x3C/script>')</script>
	<script>$.fn.modal || document.write('<script src="assets/js/bootstrap.min.js">\x3C/script>')</script>
	
	<!-- SLICK JS -->
	<link rel="stylesheet" href="assets/js/slick/slick.css">
	<link rel="stylesheet" href="assets/js/slick/slick-theme.css">
	<script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
	
	<!--lightbox-->
	<script type="text/javascript" src="assets/js/lightbox.js"></script>
	<link rel="stylesheet" href="assets/css/lightbox.css">
</head>
<body>
	<div class="header-wrapper">
		<div class="container">
			<div class="logo-wrapper">
				<img src="assets/images/logo.png" alt="Events Concierge" class="img-responsive"/>
			</div>
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <?php $base_url = 'http://localhost/events-concierge/'; ?>
				  <ul class="nav navbar-nav">
					<li><a href="<?php echo $base_url; ?>">Home</a></li>
					<li><a href="<?php echo $base_url; ?>#about">About Us</a></li>
					<li>
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Events</a>
						<div class="dropdown-menu">
						  <a class="dropdown-item" href="#weddings">Weddings</a>
						  <a class="dropdown-item" href="#">Debuts</a>
						</div>
					</li>
					<li><a href="<?php echo $base_url; ?>gallery">Gallery</a></li>
					<li><a href="<?php echo $base_url; ?>contact-us">Contact Us</a></li>
				  </ul>
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</div>
	<!-- content wrapper-->