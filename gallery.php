<?php include_once('header.php');?>
<div class="content-wrapper gallery">
	<div class="container">
		<h1>Photo Gallery</h1>
		<div class="gallery-section">
			<h2>WEDDING</h2>
			<div class="row items-wrapper">
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a rel="gallery-1" href="assets/images/Wedding/248630_799826493406762_5257684246064229131_n.jpg" class="img-preview" style="background-image: url(assets/images/Wedding/248630_799826493406762_5257684246064229131_n.jpg)" data-lightbox="wedding-gallery">
					</a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Wedding/312738_232109913511759_141015093_n.jpg" class="img-preview" style="background-image: url(assets/images/Wedding/312738_232109913511759_141015093_n.jpg)" data-lightbox="wedding-gallery"></a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Wedding/553015_340737405982342_1566302050_n.jpg" class="img-preview" style="background-image: url(assets/images/Wedding/553015_340737405982342_1566302050_n.jpg)" data-lightbox="wedding-gallery"></a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Wedding/10382534_307270316097660_1284936822911717864_o.jpg" class="img-preview" style="background-image: url(assets/images/Wedding/10382534_307270316097660_1284936822911717864_o.jpg)" data-lightbox="wedding-gallery"></a>
				</div>
			</div>
			<div class="row items-wrapper">
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Wedding/11182277_926989397357137_4079949568166829065_n.jpg" class="img-preview" style="background-image: url(assets/images/Wedding/11182277_926989397357137_4079949568166829065_n.jpg)" data-lightbox="wedding-gallery"></a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Wedding/IMG_3507.jpg" class="img-preview" style="background-image: url(assets/images/Wedding/IMG_3507.jpg)" data-lightbox="wedding-gallery"></a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Wedding/Echo_and_Dana_Wedding-113.jpg" class="img-preview" style="background-image: url(assets/images/Wedding/Echo_and_Dana_Wedding-113.jpg)" data-lightbox="wedding-gallery"></a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Wedding/12063736_1013895441999865_5529669215080188740_n.jpg" class="img-preview" style="background-image: url(assets/images/Wedding/12063736_1013895441999865_5529669215080188740_n.jpg)" data-lightbox="wedding-gallery"></a>
				</div>
			</div>
		</div>
		<div class="gallery-section">
			<h2>DEBUT</h2>
			<div class="row items-wrapper">
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Debut/debuts-img-1.png" class="img-preview" style="background-image: url(assets/images/Debut/debuts-img-1.png)" data-lightbox="debut-gallery"></a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Debut/debuts-img-2.png" class="img-preview" style="background-image: url(assets/images/Debut/debuts-img-2.png)" data-lightbox="debut-gallery"></a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Debut/debuts-img-3.png" class="img-preview" style="background-image: url(assets/images/Debut/debuts-img-3.png)" data-lightbox="debut-gallery"></a>
				</div>
				<div class="col-xs-12 col-sm-3 gallery-item">
					<a href="assets/images/Debut/debuts-img-4.png" class="img-preview" style="background-image: url(assets/images/Debut/debuts-img-4.png)" data-lightbox="debut-gallery"></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once('footer.php');?>