$(document).ready(function(){
	/* init home slider */
	$('.home-slider').slick({
		autoplay: true,
		arrows: false,
		dots: false,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		adaptiveHeight: true
	});
	
	/* init testimonial slider */
	$('.testimonial-slider').slick({
		autoplay: true,
		arrows: false,
		dots: false,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		adaptiveHeight: true
	});
	
	/* set active menu */
	setActiveMenu();
	
	/* home page scroll to animation */
	$(document).on('click', 'ul.nav.navbar-nav a', function () {
		var href = $(this).attr('href');
		
		$('ul.nav.navbar-nav li').removeClass('active');
		if ( /#about/.test(href) ) {
			scrollToElement('#aboutSection');
			$('ul.nav.navbar-nav li:nth-child(2)').addClass('active');
			return false;
		} else if ( /#weddings/.test(href) ) {
			scrollToElement('#weddingSection');
			$('ul.nav.navbar-nav li:nth-child(3)').addClass('active');
			return false;
		}
	});
	
	/* scroll to top */
	$(document).on( 'scroll', function() {
		if ($(window).scrollTop() > 100) {
			$('.scroll-top-wrapper').addClass('show');
		} else {
			$('.scroll-top-wrapper').removeClass('show');
		}
	});
	$('.scroll-top-wrapper').on('click', scrollToTop);
	
	/* lightbox settings */
	lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
	  'maxWidth': 700,
	  'wrapAround': true
    });
	
	/* submit contact form */
	$('#contact-form').submit(function() {
		var name = $('#contact-form #name').val();
		var email = $('#contact-form #email').val();
		var message = $('#contact-form #message').val();
		var captcha = $('#contact-form #captcha').val();
		var form_msg = $('.form-message');
		
		form_msg.html('');
		form_msg.hide();
		form_msg.removeClass('error');
		
		if ( name.length < 1 ) {
			form_msg.html('Please enter your name.');
			form_msg.addClass('error');
			form_msg.show();
			$('#contact-form #name').focus();
			return false
		} else if ( email.length < 1 ) {
			form_msg.html('Please enter your email.');
			form_msg.addClass('error');
			form_msg.show();
			$('#contact-form #email').focus();
			return false
		} else if ( message.length < 1 ) {
			form_msg.html('Please enter your message.');
			form_msg.addClass('error');
			form_msg.show();
			$('#contact-form #message').focus();
			return false
		} else if ( captcha.length < 1 ) {
			form_msg.html('Please answer the captcha provided.');
			form_msg.addClass('error');
			form_msg.show();
			$('#contact-form #captcha').focus();
			return false
		} else {
			return true;
		}
	});
});

function setActiveMenu() {
	var url = window.location.href;
	var hash = window.location.hash;
	
	$('ul.nav.navbar-nav a').each(function() {
		var href = $(this).attr('href');
		
		if ( url == href ) {
			$(this).parent().addClass('active');
		}
	});
	
	if ( hash != null && hash.length > 0 ) {
		if ( hash == '#about' ) {
			scrollToElement('#aboutSection');
			$('ul.nav.navbar-nav li:nth-child(2)').addClass('active');
		} else if ( hash == '#weddings' ) {
			scrollToElement('#weddingSection');
			$('ul.nav.navbar-nav li:nth-child(3)').addClass('active');
		}
		location.hash.replace('#','');
		location.hash = '';
		removeHash();
	}
}

function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 800, 'linear');
}

function scrollToElement(el) {
	$('html, body').animate({
		scrollTop: $(el).offset().top
	}, 1000);
}

function removeHash() { 
    var scrollV, scrollH, loc = window.location;
    if ("pushState" in history)
        history.pushState("", document.title, loc.pathname + loc.search);
    else {
        // Prevent scrolling by storing the page's current scroll offset
        scrollV = document.body.scrollTop;
        scrollH = document.body.scrollLeft;

        loc.hash = "";

        // Restore the scroll offset, should be flicker free
        document.body.scrollTop = scrollV;
        document.body.scrollLeft = scrollH;
    }
}