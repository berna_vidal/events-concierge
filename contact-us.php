<?php
session_start();

$name = '';
$email = '';
$number = '';
$message = '';
$captcha = '';
$scripts = '';

/* check if captcha is valid */
if ( isset($_POST['email']) && isset($_SESSION['captcha']) ) {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$number = $_POST['number'];
	$message = $_POST['message'];
	$captcha = $_POST['captcha'];
	
	if ( $_SESSION['captcha']['code'] == $captcha ) {
		$scripts = 'showSubmissionSuccess();';
		sendPHPMail($_POST);
	} else {
		$scripts = 'showWrongCaptcha();';
	}
}

/* init captcha */
include("/captcha/simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();

/* send email */
function sendPHPMail($data) {
	// $to = 'Events Concierge <ec.eventsconcierge@gmail.com>';
	$to = 'Keith Sasan <keithotzkie@gmail.com>';
	$from = $data['name'].' <'.$data['email'].'>';
	$subject = 'Events Concierge Inquiry';

	// Message
	$message = 'Name: '.$data['name'].'<br>';
	$message .= 'Email: '.$data['email'].'<br>';
	$message .= ( isset($data['number']) ? 'Phone: '.$data['number'].'<br><br>' : '' );
	$message .= 'Message:<br>'.$data['message'];

	// To send HTML mail, the Content-type header must be set
	$headers[] = 'MIME-Version: 1.0';
	$headers[] = 'Content-type: text/html; charset=iso-8859-1';

	// Additional headers
	$headers[] = 'From: '.$from;

	// Mail it
	mail($to, $subject, $message, implode("\r\n", $headers));
}

include_once('header.php');
?>
<div class="content-wrapper contact-us">
	<div class="container">
		<h1>Contact Us</h1>
		<div class="row">
			<div class="col-xs-12 col-sm-6 map-div">
				<div id="map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3925.3536785250417!2d123.90101414314022!3d10.313554045932188!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x19390a4043ab4d66!2sLarros+Bldg.!5e0!3m2!1sen!2sph!4v1489505961974" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDATHVx6wg_CqEmZH64sZpxE1fN0nYUNOI"></script>
				<div class="contact-information">
					<div class="row">
						<div class="col-xs-2">Address:</div>
						<div class="col-xs-10">Door 2, Larros Bldg. Corner Gorordo Ave. Kamputhaw, Cebu City</div>
					</div>
					<div class="row">
						<div class="col-xs-2">Phone: </div>
						<div class="col-xs-10">0917 309 1259 | 414 2069</div>
					</div>
					<div class="row">
						<div class="col-xs-2">Email:</div>
						<div class="col-xs-10">ec.eventsconcierge@gmail.com</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="form-message"></div>
				<form class="form-horizontal" id="contact-form" method="post">
					<div class="form-group">
						<label class="control-label col-sm-3" for="name">Name*</label>
						<div class="col-sm-9">
						  <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="<?php echo $name; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="email">Email Address*</label>
						<div class="col-sm-9">
						  <input type="email" class="form-control" id="email" name="email" placeholder="Your Email Address" value="<?php echo $email; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="number">Contact Number</label>
						<div class="col-sm-9">
						  <input type="text" class="form-control" id="number" name="number" placeholder="Your Contact Number" value="<?php echo $number; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" for="number">Message*</label>
						<div class="col-sm-9">
						  <textarea id="message" name="message" class="form-control" maxlength="2000" placeholder="Enter your message here"><?php echo $message; ?></textarea>
						</div>
					</div>
					<div class="form-group"> 
						<div class="col-md-12">
						  <!--<div class="g-recaptcha" id="captcha" data-sitekey="6Lf3DBkUAAAAANBkZnWCpE6S766SPJKNFgkKTWyi"></div>-->
						  <div class="form-captcha"><img src="<?php echo $_SESSION['captcha']['image_src']; ?>" /></div>
						</div>
						<label class="control-label col-sm-3" for="captcha">Captcha*</label>
						<div class="col-sm-9">
						  <input type="text" class="form-control" id="captcha" name="captcha" placeholder="Enter Captcha Above">
						</div>
						<div class="col-md-12 text-right">
							<button type="submit" class="btn btn-default" id="send-btn">SEND</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
jQuery(document).ready(function(){
	function showSubmissionSuccess() {
		var form_msg = jQuery('.form-message');
		
		form_msg.removeClass('error');
		form_msg.html('Message was successfully sent!');
		form_msg.show();
	}

	function showWrongCaptcha() {
		var form_msg = jQuery('.form-message');
		
		form_msg.addClass('error');
		form_msg.html('The captcha you entered is incorrect. Please try again.');
		form_msg.show();
	}
	
	<?php echo $scripts; ?>
});
</script>

<?php include_once('footer.php'); ?>