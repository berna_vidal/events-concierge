</div><!-- /.container-->
<!-- The scroll to top feature -->
<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-arrow-circle-up"></i>
	</span>
</div>
<div class="footer-div">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4 social-links">
				<p>Connect with us</p>
				<div class="social-icons">
					<a href="https://www.facebook.com/ec.eventsconcierge/" target="_BLANK"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="https://www.instagram.com/ec.eventsconcierge/" target="_BLANK"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 address">
				<p>Door 2, Larros Bldg. Corner Gorordo Ave. Kamputhaw, Cebu City</p>
				<p>Phone: <a href="tel:4142069">414 2069</a> | <a href="tel:09173091259">0917 309 1259</a></p>
				<p>Email: <a href="mailto:ec.eventsconcierge@gmail.com">ec.eventsconcierge@gmail.com</a></p>
			</div>
			<div class="col-xs-12 col-sm-4 logo">
				<img src="assets/images/logo.png" />
			</div>
		</div>
	</div>
</div>
<div class="copyright-div">
	<p>&copy; Copyright <?php echo date('Y'); ?></p>
</div>
<script type="text/javascript" src="assets/js/scripts.js"></script>
</body>
</html>